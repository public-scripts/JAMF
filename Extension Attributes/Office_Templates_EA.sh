#!/bin/bash -x

###############################################################################
#
# Name: Office_Template_EA.sh
# Version: 1.0
# Create Date:  01 February 2017
# Last Modified: 26 June 2017
#
# Author:  Adam Shuttleworth
# Purpose: This script checks if Company Office template files have been copied 
#		   to proper location on each user's computer.
#
###############################################################################


user=`/bin/ls -l /dev/console | /usr/bin/awk '{ print $3 }'`

pptTemp="/Users/$user/Library/Group Containers/UBF8T346G9.Office/User Content.localized/Templates.localized/iRobot.potx"
wordTemp="/Users/$user/Library/Group Containers/UBF8T346G9.Office/User Content.localized/Templates.localized/iRobot.dotm"
    
    if [ ! -f "$pptTemp" ] || [ ! -f "$wordTemp" ]; then
       echo "<result>No</result>"
    else
        echo "<result>Yes</result>"
    fi

exit 0