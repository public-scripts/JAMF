#!/bin/sh

###############################################################################
#
# Name: VPN_Profiles_Installed_EA.sh
# Version: 1.0
# Create Date:  01 February 2017
# Last Modified: 26 June 2017
#
# Author:  Adam Shuttleworth
# Purpose: This script checks which company VPN profiles have been created. 
#		   Script checks up to 5 apps.
#
###############################################################################

# Detects all network hardware & creates services for all installed network hardware
/usr/sbin/networksetup -detectnewhardware

#IFS=$'\n'

echo "<result>`/usr/sbin/networksetup -listallnetworkservices | grep "iRobot VPN"*`</result>"
exit 0