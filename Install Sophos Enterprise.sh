#!/bin/sh

###############################################################################
#
# Name: Install Sophos Enterprise.sh
# Version: 1.0
# Create Date:  23 May 2016
# Last Modified: 2 August 2017
#
# Author:  Adam Shuttleworth
# Purpose:  Script to remove System Center Endpoint Protection and install
# Sophos Enterprise
#
###############################################################################

#Global Variables

#Hardcoded, if needed
URL=""

#Variable values from JAMF
if [[ "$URL" == "" ]] && [[ "$4" != "" ]]; then
	URL="$4"
elif [[ "$URL" == "" ]] && [[ "$4" == "" ]]; then
	echo "A Download URL was not specified in parameter 4. Script cannot execute. Exiting..."
	exit 1
fi

#Attempt to kill FW processes
/usr/bin/killall scep_gui

#Remove the varios FW installed bits and pieces
/bin/rm -rf /Applications/System\ Center\ 2012\ Endpoint\ Protection.app
/bin/rm -rf /Applications/System\ Center\ Endpoint\ Protection.app

#Tell the system to forget that FW was ever installed
/usr/sbin/pkgutil --forget com.microsoft.systemCenter2012EndpointProtection.com.microsoft.scep_daemon.pkg
/usr/sbin/pkgutil --forget com.microsoft.systemCenter2012EndpointProtection.GUI_startup.pkg
/usr/sbin/pkgutil --forget com.microsoft.systemCenter2012EndpointProtection.pkgid.pkg
/usr/sbin/pkgutil --forget com.microsoft.systemCenter2012EndpointProtection.scep_kac_64_106.pkg
/usr/sbin/pkgutil --forget com.microsoft.systemCenter2012EndpointProtection.scepbkp.pkg
/usr/sbin/pkgutil --forget com.microsoft.systemCenter2012EndpointProtection.SystemCenter2012EndpointProtection.pkg
/usr/sbin/pkgutil --forget com.microsoft.systemCenterEndpointProtection.com.microsoft.scep_daemon.pkg
/usr/sbin/pkgutil --forget com.microsoft.systemCenterEndpointProtection.GUI_startup.pkg
/usr/sbin/pkgutil --forget com.microsoft.systemCenterEndpointProtection.pkgid.pkg
/usr/sbin/pkgutil --forget com.microsoft.systemCenterEndpointProtection.scep_kac_64_106.pkg
/usr/sbin/pkgutil --forget com.microsoft.systemCenterEndpointProtection.scepbkp.pkg
/usr/sbin/pkgutil --forget com.microsoft.systemCenterEndpointProtection.SystemCenterEndpointProtection.pkg

#Install Sophos Enterprise (Latest Version)---Pulls Installer from the Sophos Cloud
mkdir /Users/Shared/Sophos
cd /Users/Shared/Sophos
rm -R Sophos*
curl -O "${URL}"
unzip SophosInstall.zip &> /dev/null
chmod -R +x /Users/Shared/Sophos/Sophos\ Installer.app/
/Users/Shared/Sophos/Sophos\ Installer.app/Contents/MacOS/Sophos\ Installer --install
rm -R /Users/Shared/Sophos*
exit