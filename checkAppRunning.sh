#!/bin/bash -x

###############################################################################
#
# Name: closeLyncPrompt.sh
# Version: 1.0
# Create Date:  01 February 2017
# Last Modified: 26 June 2017
#
# Author:  Adam Shuttleworth
# Purpose: This script checks if specified applications are open and notifies user to close apps. 
#		   Script checks up to 5 apps and then runs a JAMF Pro custom triggered policy to install an app.
#	       
#
###############################################################################

# Path of Cocoadialog application
cdPath="/path/to/cocoaDialog.app/Contents/MacOS/cocoaDialog"

## Hardcoded values for testing
customTrigger="main_skype"

apparray=(
"Microsoft Word"
"Microsoft Outlook"
"Microsoft Excel"
"Microsoft PowerPoint"
"Microsoft OneNote" 
)

## Variable values from JAMF
if [[ "$customTrigger" == "" ]] && [[ "$4" != "" ]]; then
    customTrigger="$4"
elif [[ "$customTrigger" == "" ]] && [[ "$4" == "" ]]; then
    echo "A Custom trigger was not specified in parameter 4. Script cannot execute. Exiting..."
    exit 1
fi

if [[ "$5" != "" ]]; then
    apparray+="$5"
fi
if [[ "$6" != "" ]]; then
    apparray+="$6"
fi
if [[ "$7" != "" ]]; then
    apparray+="$7"
fi
if [[ "$8" != "" ]]; then
    apparray+="$8"
fi
if [[ "$9" != "" ]]; then
    apparray+="$9"
fi
if [[ "$apparray" == "" ]]; then
    echo "At least one application to close must be defined either hardcoded into the script or in paramater 5. Script cannot execute. Exiting..."
    exit 1
fi

for arg in "${apparray[@]}"; do
    if [[ "$arg" != "" ]]; then
    	echo "$arg"
    	appCheckList+=( "$arg" )
    fi
done

echo $appCheckList

function macOSVersionCheck ()
{
	# Get the current OS version to serve as an example
	THIS_OS=$(sw_vers -productVersion)

	case $THIS_OS in
        10.[8-9]*)
            echo "OS running is less than 10.11 (${THIS_OS})."
            compatible="No"
            ;;
        10.10*)
            echo "OS running is less than 10.11 (${THIS_OS})."
            compatible="No"
            ;;
        10.1[1-2]*)
            echo "OS running is greater than 10.11 (${THIS_OS})."
            compatible="Yes"
            ;;
        *)
            echo "OS not supported"
            compatible="No"
            ;;
	esac

	case $compatible in
		Yes)
			## Otherwise, move on to checking for running Office apps
    		checkForRunningApps
    		;;
		No)
			OSText="This computer is running macOS ${THIS_OS}. 

Please upgrade this computer's operating system before continuing with this installation.

Click OK to Open Self Service to find the macOS Sierra (10.12) upgrade under the Operating System category."

    		promptUser=$("$cdPath" msgbox \
        		--title "IT Department" \
        		--text "Operating System Not Compatible" \
        		--informative-text "$OSText" \
        		--button1 "OK" \
        		--icon caution \
        		--width 400)
		
			if [ "$promptUser" == "1" ]; then
        		open -a "Self Service"
        		exit 0
    		fi
    		;;
		NotFound)
			ErrorText="macOS Version was not found properly.

Please contact the Service Desk for assistance."

    		promptUser=$("$cdPath" msgbox \
        		--title "IT Department" \
        		--text "Operating System Not Found" \
        		--informative-text "$ErrorText" \
        		--button1 "OK" \
        		--icon info \
        		--width 400)
    		;;
		esac
			
}	

function checkForRunningApps ()
{

runningAppsList=()

x=0
while read appname; do
    if [[ $(ps axc | grep "$appname") != "" ]]; then
        runningAppsList+=("${appCheckList[$x]}")
    fi
    let x=$((x+1))
done < <(printf '%s\n' "${appCheckList[@]}")


if [[ "${runningAppsList[@]}" != "" ]]; then

appListText="The following applications are running and must be closed before continuing with this installation.

$(printf '%s\n' "${runningAppsList[@]}")

Close the above applications, then click Continue."

    promptUser=$("$cdPath" msgbox \
        --title "IT Department" \
        --text "Applications must be closed" \
        --informative-text "$appListText" \
        --button1 "Continue" \
        --icon info \
        --width 400)

    if [ "$promptUser" == "1" ]; then
        checkForRunningApps
    else
        exit 0
    fi
else
    echo "No applications running to be shut down. Continuing..."

    installInProgressText="Application installation is now in progress. Please be patient as it takes some time to finish.

Please wait until you see a final \"Application Installed\" message at the end. You can dismiss this window and the installation will continue."

    "$cdPath" msgbox \
    --title "IT Department" \
    --text "Installation in progress" \
    --informative-text "$installInProgressText" \
    --icon info \
    --button1 "   OK   " \
    --timeout 300 \
    --timeout-format " " \
    --width 450 \
    --quiet &

    ## Call the install policy by manual trigger
    jamf policy -trigger "$customTrigger"
fi

}

## Get the free disk space on the internal drive
freeDiskSpace=$(df -H / | awk '{getline; print $4}' | sed 's/[A-Z]//')

## If free space is too low, alert user and exit
if [[ "$freeDiskSpace" -lt 1 ]]; then
    "$cdPath" msgbox \
    --title "IT Department" \
    --text "Not enough free disk space" \
    --informative-text "Sorry. Your Mac only has $freeDiskSpace GBs of free disk space. This installation requires at least 1 GB free to continue. Please make the additional space available required for the installation, and then try again." \
    --button1 "   OK   " \
    --icon caution \
    --width 400 \
    --quiet

    exit 0
else
    ## Otherwise, move on to checking is running OS is compatible with installation (10.11 or greater)
	macOSVersionCheck
fi